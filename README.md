# ConvoSpace

ConvoSpace is a user-friendly chat application developed using React for the front-end and Firebase for the back-end. This project showcases practical experience in full-stack web development, focusing on real-time data synchronization and user authentication.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Tech Stack](#tech-stack)
- [Installation](#installation)
- [Usage](#usage)
- [Challenges Faced](#challenges-faced)
- [Learnings](#learnings)
- [Future Scope](#future-scope)
- [License](#license)

## Introduction

ConvoSpace is designed to provide a seamless chat experience with features like real-time messaging, user authentication, and more. It integrates React and Firebase to deliver a robust and interactive user interface.

## Features

- **User Authentication:** Secure sign-up and login functionality.
- **Chat List:** View and select chat rooms.
- **Chat Detail:** Engage in conversations within selected chat rooms.
- **Real-Time Messaging:** Instant messaging updates.
- **User Management:** Manage user profiles and settings.
- **Image Sharing:** Send and receive images.
- **Emoji Support:** Use emojis in messages.
- **Block User:** Option to block users.

## Tech Stack

- **Front-End:**
  - **React:** For building the user interface.
  - **Zustand:** For state management.

- **Back-End:**
  - **Firebase:** For authentication, real-time database, and cloud storage.

## Installation

To set up ConvoSpace locally, follow these steps:

1. **Clone the repository:**

   ```bash
   git clone https://github.com/your-username/convo-space.git


## Setup Firebase

1. **Create a Firebase Project**

   Go to the [Firebase Console](https://console.firebase.google.com/) and create a new project.

2. **Configure Firebase**

   Create a `.env` file in the root directory of your project and add your Firebase configuration keys:

   ```env
   REACT_APP_FIREBASE_API_KEY=your_api_key
   REACT_APP_FIREBASE_AUTH_DOMAIN=your_auth_domain
   REACT_APP_FIREBASE_PROJECT_ID=your_project_id
   REACT_APP_FIREBASE_STORAGE_BUCKET=your_storage_bucket
   REACT_APP_FIREBASE_MESSAGING_SENDER_ID=your_messaging_sender_id
   REACT_APP_FIREBASE_APP_ID=your_app_id




## Usage

Once the application is running, you can:

- **Sign up or log in** to access the chat features.
- **Create or join chat rooms**.
- **Send and receive messages** in real-time.
- **Share images** and use **emojis** in your conversations.
- **Manage your profile** and block users if needed.

## Challenges Faced

- Identifying and resolving **bugs** related to real-time data updates.
- Handling **file uploads** efficiently.
- Implementing a **search feature** in the chat list.

## Learnings

- Gained hands-on experience with **React** and **Firebase**.
- Learned about **real-time data synchronization**.
- Mastered **application state management** using **Zustand**.
- Improved **CSS skills** for better UI design.

## Future Scope

- Adding **voice messaging** and **text conversion** features.
- Enhancing the **camera functionality**.
- Making the app **responsive** for all screen sizes.
