import React, { useEffect, useState } from "react";
import { arrayRemove, arrayUnion, doc, updateDoc, onSnapshot } from "firebase/firestore";
import { useChatStore } from "../../lib/chatStore";
import { auth, db } from "../../lib/firebase";
import { useUserStore } from "../../lib/userStore";
import "./detail.css";

const Detail = ({ setShowDetail }) => {
  const { chatId, user, isCurrentUserBlocked, isReceiverBlocked, changeBlock } = useChatStore();
  const { currentUser } = useUserStore();
  const [images, setImages] = useState([]);
  const [expandedSection, setExpandedSection] = useState(null); // Manage expanded section

  useEffect(() => {
    if (chatId) {
      const unSub = onSnapshot(doc(db, "chats", chatId), (res) => {
        const chatData = res.data();
        if (chatData?.messages) {
          const imageMessages = chatData.messages.filter(message => message.img);
          setImages(imageMessages);
        }
      });

      return () => {
        unSub();
      };
    }
  }, [chatId]);

  const handleBlock = async () => {
    if (!user) return;

    const userDocRef = doc(db, "users", currentUser.id);
    try {
      await updateDoc(userDocRef, {
        blocked: isReceiverBlocked ? arrayRemove(user.id) : arrayUnion(user.id),
      });
      changeBlock();
    } catch (err) {
      console.log(err);
    }
  };

  // Toggle the expanded section
  const toggleSection = (section) => {
    setExpandedSection(prev => (prev === section ? null : section));
  };

  return (
    <div className='detail'>
      <div className="user">
        <img src={user?.avatar || "./avatar.png"} alt="User Avatar" />
        <h2>{user?.username}</h2>
      </div>
      <div className="info">
        <div className="option">
          <div className="title" onClick={() => toggleSection('settings')}>
            <span>Chat Settings</span>
            <img src={expandedSection === 'settings' ? "./arrowDown.png" : "./arrowUp.png"} alt="" />
          </div>
          {expandedSection === 'settings' && <div className="details"> {/* Content for Chat Settings */} </div>}
        </div>
        <div className="option">
          <div className="title" onClick={() => toggleSection('privacy')}>
            <span>Privacy & help</span>
            <img src={expandedSection === 'privacy' ? "./arrowDown.png" : "./arrowUp.png"} alt="" />
          </div>
          {expandedSection === 'privacy' && <div className="details"> {/* Content for Privacy & help */} </div>}
        </div>
        <div className="option">
          <div className="title" onClick={() => toggleSection('photos')}>
            <span>Shared photos</span>
            <img src={expandedSection === 'photos' ? "./arrowDown.png" : "./arrowUp.png"} alt="" />
          </div>
          {expandedSection === 'photos' && (
            <div className="photos">
              {images.map((image, index) => (
                <div className="photoItem" key={index}>
                  <div className="photoDetail">
                    <img src={image.img} alt="Shared Image" />
                    <span>{`photo_${index + 1}.png`}</span>
                  </div>
                  <img src="./download.png" alt="Download Icon" className="icon" />
                </div>
              ))}
            </div>
          )}
        </div>
        <button onClick={handleBlock}>
          {isCurrentUserBlocked
            ? "You are Blocked!"
            : isReceiverBlocked
            ? "User blocked"
            : "Block User"}
        </button>
        <button className='logout' onClick={() => auth.signOut()}>LogOut</button>
        <button onClick={() => setShowDetail(false)}>Back to Chat</button>
      </div>
    </div>
  );
};

export default Detail;
